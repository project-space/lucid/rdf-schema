# LUCID schema version 2025.1

## Contents

This is the RDF schema and associated files and documentation for the full dataset of the LUCID project.
This serves as an extension providing additional concepts to the [SPHN 2025.1 schema](https://www.biomedit.ch/rdf/sphn-schema/sphn/2025/1).
The following folders are provided:

* [doc](doc): Pylode HTML file documenting the LUCID schema.
* [dataset](dataset): Excel document with the rdf schema of the project and associated metadata.
  + [documentation](dataset/documentation): documentation sheet of each LUCID-specific concept
* [schema](schema): The SPHN schema and LUCID extension in turtle format.
* [shacl](shacl): The SHACL shapes for data validation of LUCID data.
* [sparql](sparql): SPARQL queries to compute statistical reports on LUCID data.

## External links:

* Relevant [external terminologies](https://git.dcc.sib.swiss/project-space/lucid/external-terminologies)
* Corresponding [Data transfer request specification](https://git.dcc.sib.swiss/project-space/lucid/data-transfer/-/tree/main/data-transfer-1?ref_type=heads) repository.

