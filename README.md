# LUCID RDF schema

This repository contains the RDF schema for the LUCID project. All files in this repository have been generated using schemaforge.dcc.sib.swiss

The schema consists of the SPHN schema, along with a LUCID-specific extension. 

This repository hosts the schema for two versions of the LUCID schema:

* [test-schema](test-schema) (aka minimal dataset): aligned with the SPHN schema 2023.2 (deprecated)
* [version-1](version-1): aligned with the SPHN schema 2024.2
* [version-2](version-2): aligned with the SPHN schema 2025.1
