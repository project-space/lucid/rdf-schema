This is the RDF schema for the minimal dataset of the LUCID project.
This mostly serves as a placeholder, since the minimal dataset does not include any project-specific concepts and merely reuses the SPHN 2023.2 core dataset.

The following folders are provided:

* [doc](doc): Pylode HTML file documenting the LUCID schema.
* [schema](schema): The SPHN schema and LUCID extension in turtle format.
* [shacl](shacl): The SHACL shapes for data validation of LUCID data.
* [sparql](sparql): SPARQL queries to compute statistical reports on LUCID data.
